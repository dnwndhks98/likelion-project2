# MutsaSNS 만들기 프로젝트

## 개요

  - 회원 가입 을 하여 로그인을 통해 포스트 작성, 수정, 삭제, 리스트 기능을 사용할 수 있게 기능 구현
  - 회원 가입을 하지 못한 유저는 Get 만 가능하게 만들어 봤습니다.

## Architecture(아키텍처)
![ERD](/Architecture.png)

## Swagger 주소
```text
  http://ec2-43-201-30-222.ap-northeast-2.compute.amazonaws.com:8082/swagger-ui/index.html
```
<br>

### AWS EC2에 Docker 배포
  - 배포스크립트를 작성하여 크론탭을 이용하여 자동배포하게 하였습니다.
### Gitlab CI & Crontab CD
  - .gitlab-ci.yml 파이프 라인을 작성하여 main을 통해 들어온 푸쉬만 머지 시키도록 하였습니다.


[//]: # (# SNS 구현)

[//]: # (<br>)

[//]: # (## ERD)

[//]: # (<br>)

## Endpoint

## User

### 회원가입

`POST /api/v1/users/join`

**Request Body**
```json
{
"userName": "String",
"password": "String"
}
```

**Response Body**
```json
{
    "resultCode": "SUCCESS",
    "result": {
        "userId": 0,
        "userName": "String"
    }
}
```
<br>

### 로그인
`POST /api/v1/users/login`

**Request Body**
```json
{
"userName": "String",
"password": "String"
}
```

**Response Body**
```json
{
  "jwt": "String"
}
```

<br>

## Post
### 전체 조회
`GET /api/v1/posts`

**Response Body**
```json
{
  "result": {
    "content": [
      {
        "body": "string",
        "createdAt": "yyyy-MM-dd HH:mm:ss",
        "id": 0,
        "lastModifiedAt": "yyyy-MM-dd HH:mm:ss",
        "title": "string",
        "userName": "string"
      }
  }
}
```
<br>

### 상세 조회
`GET /api/v1/posts/{id}`

**Response Body**
```json
{
  "resultCode": "string",
  "result": {
    "body": "string",
    "createdAt": "yyyy-MM-dd HH:mm:ss",
    "id": 0,
    "lastModifiedAt": "yyyy-MM-dd HH:mm:ss",
    "title": "string",
    "userName": "string"
  }
}
```
<br>

### 작성
`POST /api/v1/posts`

**Request Body**
```json
{
    "title": "String",
    "body": "String"
}
```

**Response Body**
```json
{
    "resultCode": "SUCCESS",
    "result": {
        "message": "포스트 등록 완료",
        "postId": 0
    }
}
```
<br>

### 수정 
`PUT /api/v1/posts/{id}`

**Request Body**
```json
{
    "title": "String",
    "body": "String"
}
```

**Response Body**
```json
{
    "resultCode": "SUCCESS",
    "result": {
        "message": "포스트 수정 완료",
        "postId": 0
    }
}
```
<br>

### 삭제 
`DELETE /api/v1/posts/{id}`

**Response Body**
```json
{
    "resultCode": "SUCCESS",
    "result": {
        "message": "포스트 삭제 완료",
        "postId": 0
    }
}
```
<br>
