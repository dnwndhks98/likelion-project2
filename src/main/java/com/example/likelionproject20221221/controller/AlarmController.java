package com.example.likelionproject20221221.controller;


import com.example.likelionproject20221221.entity.dto.Response;
import com.example.likelionproject20221221.entity.dto.alarm.AlarmResponse;
import com.example.likelionproject20221221.service.AlarmService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@RequiredArgsConstructor
@Api(tags = {"Alarm API"})
@RequestMapping("/api/v1")
public class AlarmController {

    private final AlarmService alarmService;

    @GetMapping("/alarms")
    @ApiOperation(value="알람", notes="특정 사용자의 글에 대한 알림 조회할 수 있습니다.")
    public Response<Page<AlarmResponse>> alarm(Principal principal, @PageableDefault(size = 20, sort = "createdDateTime") Pageable pageable) {
        return Response.success(alarmService.findMyAlarms(principal, pageable));

    }
}
