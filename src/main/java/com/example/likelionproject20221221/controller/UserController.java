package com.example.likelionproject20221221.controller;

import com.example.likelionproject20221221.entity.dto.Response;
import com.example.likelionproject20221221.entity.dto.join.UserJoinRequest;
import com.example.likelionproject20221221.entity.dto.join.UserJoinResponse;
import com.example.likelionproject20221221.entity.dto.login.UserLoginRequest;
import com.example.likelionproject20221221.entity.dto.login.UserLoginResponse;
import com.example.likelionproject20221221.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/users")
@RequiredArgsConstructor
@Api(tags = {"User API"})
public class UserController {

    private final UserService userService;

    @PostMapping("/join")
    @ApiOperation(value="회원가입", notes="userName 과 password 를 이용해 회원가입을 할 수 있습니다.")
    public Response<UserJoinResponse> add(@RequestBody UserJoinRequest dto) {
        return userService.add(dto);

    }
    @PostMapping("/login")
    @ApiOperation(value="로그인", notes="userName 과 password 를 이용해 로그인을 할 수 있습니다.")
    public Response<UserLoginResponse> add(@RequestBody UserLoginRequest dto) {
        return userService.login(dto);
    }
}
