package com.example.likelionproject20221221.controller;

import com.example.likelionproject20221221.service.AlgorithmService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;


@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
@ApiIgnore
public class HelloController {

    private final AlgorithmService algorithmService;

    @GetMapping(value = "/hello", produces="application/json;charset=UTF-8")
    public ResponseEntity<String> hello() {
        return ResponseEntity.ok().body("우주완");
    }

    @GetMapping("/hello/{num}")
    public Integer divide(@PathVariable Integer num) {
        return algorithmService.sumOfDigit(num);
    }

}

