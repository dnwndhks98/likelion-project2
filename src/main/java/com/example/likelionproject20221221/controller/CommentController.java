package com.example.likelionproject20221221.controller;

import com.example.likelionproject20221221.entity.dto.Response;
import com.example.likelionproject20221221.entity.dto.comment.CommentDeleteResponse;
import com.example.likelionproject20221221.entity.dto.comment.CommentEditResponse;
import com.example.likelionproject20221221.entity.dto.comment.CommentRequest;
import com.example.likelionproject20221221.entity.dto.comment.CommentResponse;
import com.example.likelionproject20221221.service.CommentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.security.Principal;

@RestController
@RequestMapping("/api/v1/posts")
@RequiredArgsConstructor
@Api(tags = {"Comment API"})
public class CommentController {

    private final CommentService commentService;

    @PostMapping("/{postId}/comments")
    @ApiOperation(value="댓글 등록", notes="로그인 후 포스트에 댓글을 등록 할 수 있습니다.")
    public Response<CommentResponse> create(@RequestBody CommentRequest dto, @PathVariable Long postId, @ApiIgnore Principal principal) {
        CommentResponse commentResponse = commentService.create(dto, postId, principal);
        return Response.success(commentResponse);
    }

    @PutMapping("/{postId}/comments/{id}")
    @ApiOperation(value="댓글 수정", notes="로그인 후 나의 댓글을 수정 할 수 있습니다.")
    public Response<CommentEditResponse> update(@RequestBody CommentRequest dto, @PathVariable Long postId, @ApiIgnore Principal principal, @PathVariable Long id) {
        CommentEditResponse commentEditResponse = commentService.update(dto, postId, principal, id);
        return Response.success(commentEditResponse);
    }

    @GetMapping("/{postId}/comments")
    @ApiOperation(value="댓글 조회", notes="해당 포스터 댓글 정보를 조회할 수 있습니다.")
    public Response<Page<CommentResponse>> read(@PageableDefault(size = 10, sort = "createdDateTime", direction = Sort.Direction.DESC) Pageable pageable, @PathVariable Long postId) {
        Page<CommentResponse> commentList = commentService.findAll(pageable, postId);
        return Response.success(commentList);
    }

    @DeleteMapping("/{postId}/comments/{id}")
    @ApiOperation(value="댓글 삭제", notes="로그인 후 나의 댓글을 삭제 할 수 있습니다.")
    public Response<CommentDeleteResponse> delete(@PathVariable Long postId, @ApiIgnore Principal principal, @PathVariable Long id) {
        CommentResponse commentResponse = commentService.delete(postId, principal,id);
        return Response.success(CommentDeleteResponse.success("댓글 삭제 완료", commentResponse));
    }

}
