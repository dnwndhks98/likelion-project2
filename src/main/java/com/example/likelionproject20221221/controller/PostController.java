package com.example.likelionproject20221221.controller;


import com.example.likelionproject20221221.entity.dto.Response;
import com.example.likelionproject20221221.entity.dto.post.PostCreateResponse;
import com.example.likelionproject20221221.entity.dto.post.PostRequest;
import com.example.likelionproject20221221.entity.dto.post.PostResponse;
import com.example.likelionproject20221221.service.PostService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.security.Principal;

@RestController
@RequestMapping("/api/v1/posts")
@RequiredArgsConstructor
@Api(tags = {"Post API"})
public class PostController {

    private final PostService postService;


    @PostMapping("")
    @ApiOperation(value="포스트 작성", notes="로그인을 하면 포스트를 작성할 수 있습니다.")
    public Response<PostCreateResponse> add(@ApiIgnore Principal principal, @RequestBody PostRequest dto) {
        PostResponse postResponse = postService.add(principal, dto);
        return Response.success(PostCreateResponse.success("포스트 등록 완료", postResponse));
    }

    @PutMapping("/{id}")
    @ApiOperation(value="포스트 수정", notes="로그인을 하면 나의 포스트를 수정할 수 있습니다.")
    public Response<PostCreateResponse> edit(@PathVariable("id") Long id, @ApiIgnore Principal principal, @RequestBody PostRequest dto) {
        PostResponse postResponse = postService.edit(id, principal, dto);
        return Response.success(PostCreateResponse.success("포스트 수정 완료", postResponse));
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value="포스트 삭제", notes="로그인을 하면 나의 포스트를 삭제할 수 있습니다.")
    public Response<PostCreateResponse> delete(@PathVariable("id") Long id, @ApiIgnore Principal principal) {
        PostResponse postResponse = postService.delete(id, principal);
        return Response.success(PostCreateResponse.success("포스트 삭제 완료", postResponse));
    }


    @GetMapping("/{id}")
    @ApiOperation(value="포스트 상세조회", notes="포스트를 상제정보를 조회할 수 있습니다.")
    public Response<PostResponse> findById(@PathVariable("id") Long id) {
        return Response.success(postService.findById(id));
    }

    @GetMapping("")
    @ApiOperation(value="포스트 조회", notes="전체 포스트를 정보를 조회할 수 있습니다.")
    public Response<Page<PostResponse>> findAll(Pageable pageable) {
        return Response.success(postService.findAll(pageable));
    }


    /**
     *  like
     */

    @PostMapping("/{postId}/likes")
    @ApiOperation(value="좋아요 누르기", notes="해당 포스트에 좋아요를 누를 수 있습니다.")
    public Response<String> likePush(@PathVariable Long postId, Principal principal) {
        return Response.success(postService.likePush(postId, principal));
    }

    @GetMapping("/{postId}/likes")
    @ApiOperation(value="좋아요 조회", notes="해당 포스트에 대한 좋아요 개수를 조회할 수 있습니다.")
    public Response<Integer> likeCount(@PathVariable Long postId) {
        return Response.success(postService.countLikes(postId));
    }


    /**
     * 마이피드
     */

    @GetMapping("/my")
    @ApiOperation(value="마이피드", notes="로그인된 유저만의 피드목록을 조회 할 수 있습니다.")
    public Response<Page<PostResponse>> myFeed(Principal principal,@PageableDefault(size = 20, sort = "createdDateTime") Pageable pageable) {
        return Response.success(postService.myFeed(principal, pageable));
    }
}
