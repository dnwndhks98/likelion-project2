package com.example.likelionproject20221221.entity.dto.post;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class PostCreateResponse {
    private String message;
    private Long postId;


    public static <T> PostCreateResponse success (String message, PostResponse postResponse) {
        return new PostCreateResponse(message, postResponse.getId());
    }

}


