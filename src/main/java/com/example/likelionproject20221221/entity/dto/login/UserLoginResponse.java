package com.example.likelionproject20221221.entity.dto.login;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class UserLoginResponse {
    private String jwt;

    public UserLoginResponse(String jwt) {
        this.jwt = jwt;
    }
}
