package com.example.likelionproject20221221.entity.dto.alarm;

import com.example.likelionproject20221221.entity.Alarm;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AlarmResponse {
    private Long id;
    private String fromUserId;
    private String message;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createdAt;


    public static AlarmResponse of(Alarm alarm) {
        return AlarmResponse.builder()
                .id(alarm.getId())
                .fromUserId(alarm.getFromUser().getUserName())
                .message(alarm.getAlarmType().getMessage())
                .createdAt(alarm.getCreatedDateTime())
                .build();
    }


}
