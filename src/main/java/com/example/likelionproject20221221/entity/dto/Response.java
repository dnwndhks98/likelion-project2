package com.example.likelionproject20221221.entity.dto;

import lombok.Getter;

@Getter
public class Response<T> {
    private String resultCode;
    private T result;

    public Response(String resultCode, T result) {
        this.resultCode = resultCode;
        this.result = result;
    }

    public static <T> Response<T> error(String resultCode, T result) {
        return new Response(resultCode, result);
    }

    public static <T> Response<T> success (T result) {
        return new Response("SUCCESS", result);
    }

}
