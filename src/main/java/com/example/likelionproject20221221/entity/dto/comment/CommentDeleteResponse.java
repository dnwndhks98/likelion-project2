package com.example.likelionproject20221221.entity.dto.comment;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class CommentDeleteResponse {
    private String message;
    private Long id;


    public static <T> CommentDeleteResponse success (String message, CommentResponse commentResponse) {
        return new CommentDeleteResponse(message, commentResponse.getId());
    }

}


