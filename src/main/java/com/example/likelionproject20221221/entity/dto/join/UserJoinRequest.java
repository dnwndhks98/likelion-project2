package com.example.likelionproject20221221.entity.dto.join;


import com.example.likelionproject20221221.entity.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UserJoinRequest {
    private String userName;
    private String password;

    public User toEntity(String encoded) {
        return User.builder()
                .userName(userName)
                .password(encoded)
                .build();
    }

}
