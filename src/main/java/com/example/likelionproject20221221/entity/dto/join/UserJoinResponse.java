package com.example.likelionproject20221221.entity.dto.join;

import com.example.likelionproject20221221.entity.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UserJoinResponse {
    private Long userId;
    private String userName;

    public static UserJoinResponse of(User user) {
        return new UserJoinResponse(user.getUserId(), user.getUserName());
    }
}
