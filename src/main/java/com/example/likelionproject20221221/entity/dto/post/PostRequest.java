package com.example.likelionproject20221221.entity.dto.post;


import com.example.likelionproject20221221.entity.Post;
import com.example.likelionproject20221221.entity.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class PostRequest {

    private String title;
    private String body;

//    public Post toEntity() {
//        return Post.builder()
//                .title(title)
//                .body(body)
//                .build();
//    }
    public Post toEntity(User user) {
        return Post.builder()
                .title(title)
                .body(body)
                .user(user)
                .build();
    }

}
