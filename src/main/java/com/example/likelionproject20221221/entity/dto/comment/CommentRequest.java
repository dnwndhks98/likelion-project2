package com.example.likelionproject20221221.entity.dto.comment;

import com.example.likelionproject20221221.entity.Comment;
import com.example.likelionproject20221221.entity.Post;
import com.example.likelionproject20221221.entity.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Getter
@NoArgsConstructor
public class CommentRequest {

    private String comment;

    public Comment toEntity(Post post, User user) {
        return Comment.builder()
                .comment(comment)
                .post(post)
                .user(user)
                .build();
    }
}
