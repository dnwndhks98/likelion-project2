package com.example.likelionproject20221221.repository;

import com.example.likelionproject20221221.entity.Likes;
import com.example.likelionproject20221221.entity.Post;
import com.example.likelionproject20221221.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface LikeRepository extends JpaRepository<Likes, Long> {
    Optional<Likes> findByPostAndUser(Post post, User user);

    Integer countByPost(Post post);
}
