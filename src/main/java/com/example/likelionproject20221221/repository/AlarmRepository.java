package com.example.likelionproject20221221.repository;

import com.example.likelionproject20221221.entity.Alarm;
import com.example.likelionproject20221221.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AlarmRepository extends JpaRepository<Alarm, Long> {
    Page<Alarm> findByTargetUser(User user, Pageable pageable);
}
