package com.example.likelionproject20221221.repository;

import com.example.likelionproject20221221.entity.Comment;
import com.example.likelionproject20221221.entity.Post;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommentRepository extends JpaRepository<Comment, Long> {
    Page<Comment> findAllByPost(Pageable pageable, Post post);
}
