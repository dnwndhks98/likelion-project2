package com.example.likelionproject20221221.service;

import com.example.likelionproject20221221.entity.Likes;
import com.example.likelionproject20221221.entity.Post;
import com.example.likelionproject20221221.entity.User;
import com.example.likelionproject20221221.entity.dto.post.PostRequest;
import com.example.likelionproject20221221.entity.dto.post.PostResponse;
import com.example.likelionproject20221221.exception.AppException;
import com.example.likelionproject20221221.exception.ErrorCode;
import com.example.likelionproject20221221.repository.LikeRepository;
import com.example.likelionproject20221221.repository.PostRepository;
import com.example.likelionproject20221221.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.Principal;

@Service
@RequiredArgsConstructor
@Slf4j
public class PostService {

    private final PostRepository postRepository;
    private final UserRepository userRepository;
    private final LikeRepository likeRepository;


    // 포스트 추가
    // Principal 인증된 토큰을 가지고 getName 을 해준다.
    public PostResponse add(Principal principal, PostRequest dto) {
        log.info("principal : {} ",principal.getName());
        User user = userRepository.findByUserName(principal.getName()).orElseThrow(()->new AppException(ErrorCode.USERNAME_NOT_FOUND, ErrorCode.USERNAME_NOT_FOUND.getMessage()));
        Post post = postRepository.save(dto.toEntity(user));
        return PostResponse.of(post);
    }

    // 포스트 수정
    @Transactional
    public PostResponse edit(Long postId, Principal principal, PostRequest dto) {

        Post post = postRepository.findById(postId).orElseThrow(()->new AppException(ErrorCode.POST_NOT_FOUND, ErrorCode.POST_NOT_FOUND.getMessage()));

        User user = userRepository.findByUserName(principal.getName()).orElseThrow(() -> new AppException(ErrorCode.USERNAME_NOT_FOUND, ErrorCode.USERNAME_NOT_FOUND.getMessage()));

        log.info("post.getId : {}, user.getUserId : {} ",post.getId(), user.getUserId());
        if (!post.getUser().getUserId().equals(user.getUserId()))
            throw new AppException(ErrorCode.INVALID_TOKEN, ErrorCode.INVALID_TOKEN.getMessage());

        post.update(dto.getTitle(), dto.getBody());
        return PostResponse.of(post);
    }

    // 포스트 삭제
    public PostResponse delete(Long postId, Principal principal) {
        Post post = postRepository.findById(postId).orElseThrow(()->new AppException(ErrorCode.POST_NOT_FOUND, ErrorCode.POST_NOT_FOUND.getMessage()));

        User user = userRepository.findByUserName(principal.getName()).orElseThrow(()->new AppException(ErrorCode.USERNAME_NOT_FOUND, ErrorCode.USERNAME_NOT_FOUND.getMessage()));

        if (!post.getUser().getUserId().equals(user.getUserId())) throw new AppException(ErrorCode.INVALID_TOKEN, ErrorCode.INVALID_TOKEN.getMessage());

        postRepository.deleteById(postId);
        return PostResponse.of(post);
    }

    //포스트 아이디로 찾기
    public PostResponse findById(Long postId) {
        Post post = postRepository.findById(postId).orElseThrow(()->new AppException(ErrorCode.POST_NOT_FOUND, ErrorCode.POST_NOT_FOUND.getMessage()));
        return PostResponse.of(post);
    }

    public Page<PostResponse> findAll(Pageable pageable) {
        return postRepository.findAll(pageable).map(PostResponse::of);
    }

    // 좋아요 눌르기
    public String likePush(Long postId, Principal principal) {
        Post post = postRepository.findById(postId).orElseThrow(()->new AppException(ErrorCode.POST_NOT_FOUND, ErrorCode.POST_NOT_FOUND.getMessage()));

        User user = userRepository.findByUserName(principal.getName()).orElseThrow(()->new AppException(ErrorCode.USERNAME_NOT_FOUND, ErrorCode.USERNAME_NOT_FOUND.getMessage()));

        likeRepository.findByPostAndUser(post,user).ifPresent(Likes -> {
            throw new AppException(ErrorCode.DUPLICATED_LIKE, ErrorCode.DUPLICATED_LIKE.getMessage());
        });

        Likes likes = Likes.builder()
                .post(post)
                .user(user)
                .build();

        likeRepository.save(likes);


        return "좋아요를 눌렀습니다.";
    }

    //좋아요 개수 확인
    public Integer countLikes(Long postId) {
        Post post = postRepository.findById(postId).orElseThrow(()->new AppException(ErrorCode.POST_NOT_FOUND, ErrorCode.POST_NOT_FOUND.getMessage()));
        return likeRepository.countByPost(post);
    }

    //마이 피드 조회
    public Page<PostResponse> myFeed(Principal principal, Pageable pageable) {
        User user = userRepository.findByUserName(principal.getName()).orElseThrow(()->new AppException(ErrorCode.USERNAME_NOT_FOUND, ErrorCode.USERNAME_NOT_FOUND.getMessage()));
        return postRepository.findAllByUser(user, pageable).map(PostResponse::of);
    }
}
