package com.example.likelionproject20221221.service;

import com.example.likelionproject20221221.entity.Comment;
import com.example.likelionproject20221221.entity.Post;
import com.example.likelionproject20221221.entity.User;
import com.example.likelionproject20221221.entity.dto.comment.CommentEditResponse;
import com.example.likelionproject20221221.entity.dto.comment.CommentRequest;
import com.example.likelionproject20221221.entity.dto.comment.CommentResponse;
import com.example.likelionproject20221221.exception.AppException;
import com.example.likelionproject20221221.exception.ErrorCode;
import com.example.likelionproject20221221.repository.CommentRepository;
import com.example.likelionproject20221221.repository.PostRepository;
import com.example.likelionproject20221221.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.Principal;

@Service
@RequiredArgsConstructor
public class CommentService {

    private final CommentRepository commentRepository;
    private final PostRepository postRepository;
    private final UserRepository userRepository;

    public CommentResponse create(CommentRequest dto, Long id, Principal principal) {

        Post post = postRepository.findById(id).orElseThrow(() -> new AppException(ErrorCode.POST_NOT_FOUND, ErrorCode.POST_NOT_FOUND.getMessage()));

        User user = userRepository.findByUserName(principal.getName()).orElseThrow(() -> new AppException(ErrorCode.USERNAME_NOT_FOUND, ErrorCode.USERNAME_NOT_FOUND.getMessage()));

        if (!post.getUser().getUserId().equals(user.getUserId()))
            throw new AppException(ErrorCode.INVALID_TOKEN, ErrorCode.INVALID_TOKEN.getMessage());

        Comment comment = commentRepository.save(dto.toEntity(post, user));
        return CommentResponse.of(comment);
    }

    @Transactional
    public CommentEditResponse update(CommentRequest dto, Long postId, Principal principal, Long id) {

        Comment comment = commentRepository.findById(id).orElseThrow(() -> new AppException(ErrorCode.COMMENT_NOT_FOUND, ErrorCode.COMMENT_NOT_FOUND.getMessage()));

        Post post = postRepository.findById(postId).orElseThrow(() -> new AppException(ErrorCode.POST_NOT_FOUND, ErrorCode.POST_NOT_FOUND.getMessage()));

        User user = userRepository.findByUserName(principal.getName()).orElseThrow(() -> new AppException(ErrorCode.USERNAME_NOT_FOUND, ErrorCode.USERNAME_NOT_FOUND.getMessage()));

        if (!post.getUser().getUserId().equals(user.getUserId()))
            throw new AppException(ErrorCode.INVALID_TOKEN, ErrorCode.INVALID_TOKEN.getMessage());


        comment.update(dto.getComment());

        return CommentEditResponse.of(comment);
    }

    public Page<CommentResponse> findAll(Pageable pageable, Long postId) {
        Post post = postRepository.findById(postId).orElseThrow(() -> new AppException(ErrorCode.POST_NOT_FOUND, ErrorCode.POST_NOT_FOUND.getMessage()));
        return commentRepository.findAllByPost(pageable, post).map(CommentResponse::of);
    }

    public CommentResponse delete(Long postId, Principal principal, Long id) {

        Comment comment = commentRepository.findById(id).orElseThrow(() -> new AppException(ErrorCode.COMMENT_NOT_FOUND, ErrorCode.COMMENT_NOT_FOUND.getMessage()));

        Post post = postRepository.findById(postId).orElseThrow(() -> new AppException(ErrorCode.POST_NOT_FOUND, ErrorCode.POST_NOT_FOUND.getMessage()));

        User user = userRepository.findByUserName(principal.getName()).orElseThrow(() -> new AppException(ErrorCode.USERNAME_NOT_FOUND, ErrorCode.USERNAME_NOT_FOUND.getMessage()));

        if (!post.getUser().getUserId().equals(user.getUserId()))
            throw new AppException(ErrorCode.INVALID_TOKEN, ErrorCode.INVALID_TOKEN.getMessage());

        commentRepository.deleteById(id);
        return CommentResponse.of(comment);
    }

}
