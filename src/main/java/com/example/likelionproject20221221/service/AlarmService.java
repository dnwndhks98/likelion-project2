package com.example.likelionproject20221221.service;

import com.example.likelionproject20221221.entity.Alarm;
import com.example.likelionproject20221221.entity.User;
import com.example.likelionproject20221221.entity.dto.alarm.AlarmResponse;
import com.example.likelionproject20221221.exception.AppException;
import com.example.likelionproject20221221.exception.ErrorCode;
import com.example.likelionproject20221221.repository.AlarmRepository;
import com.example.likelionproject20221221.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.security.Principal;

@RequiredArgsConstructor
@Service
public class AlarmService {

    private final UserRepository userRepository;
    private final AlarmRepository alarmRepository;


    public Page<AlarmResponse> findMyAlarms(Principal principal, Pageable pageable) {
        User user = userRepository.findByUserName(principal.getName()).orElseThrow(()->new AppException(ErrorCode.USERNAME_NOT_FOUND, ErrorCode.USERNAME_NOT_FOUND.getMessage()));
        return alarmRepository.findByTargetUser(user, pageable).map(AlarmResponse::of);
    }
}
