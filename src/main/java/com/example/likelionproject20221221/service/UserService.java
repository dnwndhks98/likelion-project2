package com.example.likelionproject20221221.service;

import com.example.likelionproject20221221.entity.User;
import com.example.likelionproject20221221.entity.dto.Response;
import com.example.likelionproject20221221.entity.dto.join.UserJoinRequest;
import com.example.likelionproject20221221.entity.dto.join.UserJoinResponse;
import com.example.likelionproject20221221.entity.dto.login.UserLoginRequest;
import com.example.likelionproject20221221.entity.dto.login.UserLoginResponse;
import com.example.likelionproject20221221.exception.AppException;
import com.example.likelionproject20221221.exception.ErrorCode;
import com.example.likelionproject20221221.repository.UserRepository;
import com.example.likelionproject20221221.security.utils.JwtTokenUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserService {

    private final UserRepository userRepository;

    private final BCryptPasswordEncoder encoder;

    @Value("${jwt.token.secret}")
    private String key;

    private Long expireTimesMs = 1000 * 60 * 60L; // 1000*60*60 = 1시간

    public Response<UserJoinResponse> add(UserJoinRequest dto) {

        // userName 중복
        userRepository.findByUserName(dto.getUserName()).ifPresent(user -> {
            throw new AppException(ErrorCode.DUPLICATED_USERNAME,ErrorCode.DUPLICATED_USERNAME.getMessage());
        });

//        User user = User.builder()
//                .userName(dto.getUserName())
//                .password(encoder.encode(dto.getPassword()))
//                .build();
        String encodePw = encoder.encode(dto.getPassword());
        User user = userRepository.save(dto.toEntity(encodePw));

        return Response.success(UserJoinResponse.of(user));
    }


    public Response<UserLoginResponse> login(UserLoginRequest dto) {
        // userName 찾을 수 없음
        User selectedUser = userRepository.findByUserName(dto.getUserName())
                .orElseThrow(()-> new AppException(ErrorCode.USERNAME_NOT_FOUND, ErrorCode.USERNAME_NOT_FOUND.getMessage()));

        // password 틀림
        // encoder 완료
        log.info("selectedPw : {}, pw : {}", selectedUser.getPassword(), dto.getPassword());
        if (!encoder.matches(dto.getPassword(), selectedUser.getPassword())) {
            throw new AppException(ErrorCode.INVALID_PASSWORD, ErrorCode.INVALID_PASSWORD.getMessage());
        }

        String token = JwtTokenUtil.createToken(selectedUser.getUserName(), key, expireTimesMs);
        return Response.success(new UserLoginResponse(token));
    }


}
