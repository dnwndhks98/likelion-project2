package com.example.likelionproject20221221;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class LikelionProject20221221Application {

    public static void main(String[] args) {
        SpringApplication.run(LikelionProject20221221Application.class, args);
    }

}
