package com.example.likelionproject20221221.controller;

import com.example.likelionproject20221221.entity.dto.comment.CommentEditResponse;
import com.example.likelionproject20221221.entity.dto.comment.CommentRequest;
import com.example.likelionproject20221221.entity.dto.comment.CommentResponse;
import com.example.likelionproject20221221.exception.AppException;
import com.example.likelionproject20221221.exception.ErrorCode;
import com.example.likelionproject20221221.security.configuration.EncoderConfig;
import com.example.likelionproject20221221.service.CommentService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.jpa.mapping.JpaMetamodelMappingContext;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(CommentController.class)
@MockBean(JpaMetamodelMappingContext.class)
class CommentControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    CommentService commentService;

    @MockBean
    EncoderConfig encoderConfig;

    @Autowired
    ObjectMapper objectMapper;

    String comment = "댓글";
    String commentUpdate = "댓글 수정";

    CommentRequest commentRequest = new CommentRequest(comment);
    CommentRequest commentUpdateRequest = new CommentRequest(commentUpdate);

    CommentResponse commentResponse = CommentResponse.builder()
            .id(1L)
            .comment(commentRequest.getComment())
            .userName("이름")
            .postId(1L)
            .createdAt(LocalDateTime.now())
            .build();

    CommentEditResponse commentEditResponse = CommentEditResponse.builder()
            .id(1L)
            .comment(commentUpdateRequest.getComment())
            .userName("이름")
            .postId(1L)
            .createdAt(LocalDateTime.now())
            .lastModifiedAt(LocalDateTime.now())
            .build();



    @Test
    @DisplayName("댓글 등록 - 성공")
    @WithMockUser
    void create_success() throws Exception {
        //when
        when(commentService.create(any(), any(), any()))
                .thenReturn(commentResponse);

        //then
        mockMvc.perform(post("/api/v1/posts/1/comments")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(commentRequest)))
                .andDo(print())
                .andExpect(jsonPath("$.resultCode").exists())
                .andExpect(jsonPath("$.result.id").exists())
                .andExpect(jsonPath("$.result.comment").exists())
                .andExpect(jsonPath("$.result.userName").exists())
                .andExpect(jsonPath("$.result.postId").exists())
                .andExpect(jsonPath("$.result.createdAt").exists())
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("댓글 등록 - 실패(해당 포스트 없을 때)")
    @WithMockUser
    void create_fail_1() throws Exception {
        //when
        when(commentService.create(any(), any(), any()))
                .thenThrow(new AppException(ErrorCode.POST_NOT_FOUND, ErrorCode.POST_NOT_FOUND.getMessage()));

        //then
        mockMvc.perform(post("/api/v1/posts/1/comments")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(commentRequest)))
                .andDo(print())
                .andExpect(jsonPath("$.resultCode").exists())
                .andExpect(jsonPath("$.result.errorCode").exists())
                .andExpect(jsonPath("$.result.message").exists())
                .andExpect(status().is(ErrorCode.POST_NOT_FOUND.getHttpStatus().value()));
    }


    @Test
    @DisplayName("댓글 수정 - 성공")
    @WithMockUser
    void create_update_success() throws Exception {
        //when
        when(commentService.update(any(), any(), any(), any()))
                .thenReturn(commentEditResponse);

        //then
        mockMvc.perform(put("/api/v1/posts/1/comments/1")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(commentUpdateRequest)))
                .andDo(print())
                .andExpect(jsonPath("$.resultCode").exists())
                .andExpect(jsonPath("$.result.id").exists())
                .andExpect(jsonPath("$.result.comment").value("댓글 수정"))
                .andExpect(jsonPath("$.result.userName").exists())
                .andExpect(jsonPath("$.result.postId").exists())
                .andExpect(jsonPath("$.result.createdAt").exists())
                .andExpect(jsonPath("$.result.lastModifiedAt").exists())
                .andExpect(status().isOk());
    }


    @Test
    @DisplayName("댓글 수정 - 실패(해당 포스트 없을 때)")
    @WithMockUser
    void create_update_fail_1() throws Exception {
        //when
        when(commentService.create(any(), any(), any()))
                .thenThrow(new AppException(ErrorCode.POST_NOT_FOUND, ErrorCode.POST_NOT_FOUND.getMessage()));

        //then
        mockMvc.perform(post("/api/v1/posts/1/comments")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(commentRequest)))
                .andDo(print())
                .andExpect(jsonPath("$.resultCode").exists())
                .andExpect(jsonPath("$.result.errorCode").exists())
                .andExpect(jsonPath("$.result.message").exists())
                .andExpect(status().is(ErrorCode.POST_NOT_FOUND.getHttpStatus().value()));
    }
}