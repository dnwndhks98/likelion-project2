package com.example.likelionproject20221221.controller;

import com.example.likelionproject20221221.entity.dto.Response;
import com.example.likelionproject20221221.entity.dto.join.UserJoinRequest;
import com.example.likelionproject20221221.entity.dto.join.UserJoinResponse;
import com.example.likelionproject20221221.entity.dto.login.UserLoginRequest;
import com.example.likelionproject20221221.entity.dto.login.UserLoginResponse;
import com.example.likelionproject20221221.exception.AppException;
import com.example.likelionproject20221221.exception.ErrorCode;
import com.example.likelionproject20221221.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.jpa.mapping.JpaMetamodelMappingContext;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(UserController.class)
@MockBean(JpaMetamodelMappingContext.class)
class UserControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    UserService userService;

    @Autowired
    ObjectMapper objectMapper; // java object 를 json 형식으로 바꿔주는 잭슨의 오브젝트 이다


    // given
    String userName = "juwan";
    String password = "q1w2e3r4t5";
    String token = "token";


    UserJoinRequest userJoinRequest = new UserJoinRequest(userName, password);
    UserLoginRequest userLoginRequest = new UserLoginRequest(userName, password);

    @Test
    @DisplayName("회원 가입 join 성공 테스트")
    @WithMockUser
    void join() throws Exception {

        UserJoinResponse userJoinResponse = UserJoinResponse.of(userJoinRequest.toEntity(password));

        // when
        when(userService.add(any()))
                .thenReturn(Response.success(userJoinResponse));

        mockMvc.perform(post("/api/v1/users/join")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON) // type json
                        .content(objectMapper.writeValueAsBytes(userJoinRequest)))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("회원가입 실패 - userName 중복")
    @WithMockUser
    void join_fail() throws Exception {


        // when
        when(userService.add(any()))
                .thenThrow(new AppException(ErrorCode.DUPLICATED_USERNAME, ErrorCode.DUPLICATED_USERNAME.getMessage()));


        // then
        mockMvc.perform(post("/api/v1/users/join")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(userJoinRequest)))
                .andDo(print())
                .andExpect(status().is(ErrorCode.DUPLICATED_USERNAME.getHttpStatus().value()));

    }

    @Test
    @DisplayName("로그인 성공")
    @WithMockUser
    void login() throws Exception {

        // when
        when(userService.login(any())).thenReturn(Response.success(new UserLoginResponse(token)));


        // then
        mockMvc.perform(post("/api/v1/users/login")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(userLoginRequest)))
                .andDo(print())
                .andExpect(jsonPath("$.resultCode").exists())
                .andExpect(jsonPath("$.result.jwt").exists());
    }

    @Test
    @DisplayName("로그인 실패_아이디 찾을 수 없음")
    @WithMockUser
    void login_fail_1() throws Exception {

        // when
        when(userService.login(any()))
                .thenThrow(new AppException(ErrorCode.USERNAME_NOT_FOUND, ErrorCode.USERNAME_NOT_FOUND.getMessage()));


        // then
        mockMvc.perform(post("/api/v1/users/login")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(userLoginRequest)))
                .andDo(print())
                .andExpect(status().is(ErrorCode.USERNAME_NOT_FOUND.getHttpStatus().value()));
    }

    @Test
    @DisplayName("로그인 실패_비밀번호 틀림")
    @WithMockUser
    void login_fail_2() throws Exception {

        // when
        when(userService.login(any()))
                .thenThrow(new AppException(ErrorCode.INVALID_PASSWORD, ErrorCode.INVALID_PASSWORD.getMessage()));


        // then
        mockMvc.perform(post("/api/v1/users/login")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(userLoginRequest)))
                .andDo(print())
                .andExpect(status().is(ErrorCode.INVALID_PASSWORD.getHttpStatus().value()));
    }
}