package com.example.likelionproject20221221.controller;

import com.example.likelionproject20221221.entity.dto.post.PostRequest;
import com.example.likelionproject20221221.entity.dto.post.PostResponse;
import com.example.likelionproject20221221.exception.AppException;
import com.example.likelionproject20221221.exception.ErrorCode;
import com.example.likelionproject20221221.security.configuration.EncoderConfig;
import com.example.likelionproject20221221.service.PostService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.jpa.mapping.JpaMetamodelMappingContext;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(PostController.class)
@MockBean(JpaMetamodelMappingContext.class)
class PostControllerTest {

    @Autowired
    MockMvc mockMvc;
    @MockBean
    PostService postService;

    @MockBean
    EncoderConfig encoderConfig;

    @Autowired
    ObjectMapper objectMapper;
    String title = "제목";
    String body = "내용내용";

    PostRequest postRequest = new PostRequest(title, body);
//    PostResponse postResponse = PostResponse.of(postRequest.toEntity());

    // given
    PostResponse postResponse = PostResponse.builder()
            .id(1L)
            .title(title)
            .body(body)
            .userName("이름")
            .createdAt(LocalDateTime.now())
            .lastModifiedAt(LocalDateTime.now())
            .build();

    @Test
    @DisplayName("상세 조회")
    @WithMockUser
    void findById() throws Exception {

        // when
        when(postService.findById(any()))
                .thenReturn(postResponse);

        // then
        mockMvc.perform(get("/api/v1/posts/1")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(jsonPath("$.resultCode").exists())
                .andExpect(jsonPath("$.result.id").exists())
                .andExpect(jsonPath("$.result.title").exists())
                .andExpect(jsonPath("$.result.body").exists())
                .andExpect(jsonPath("$.result.userName").exists())
                .andExpect(jsonPath("$.result.createdAt").exists())
                .andExpect(jsonPath("$.result.lastModifiedAt").exists())
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("post 작성 성공 테스트")
    @WithMockUser
    void post_add() throws Exception {

        // when
        when(postService.add(any(), any()))
                .thenReturn(postResponse);

        // then
        mockMvc.perform(post("/api/v1/posts")
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(postRequest)))
                .andDo(print())
                .andExpect(jsonPath("$.result.message").exists())
                .andExpect(jsonPath("$.result.postId").exists())
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("post 작성 실패 테스트_잘못된 토큰")
    @WithMockUser
    void post_add_fail_1() throws Exception {

        // when
        when(postService.add(any(), any()))
                .thenThrow(new AppException(ErrorCode.INVALID_TOKEN, ErrorCode.INVALID_TOKEN.getMessage()));

        // then
        mockMvc.perform(post("/api/v1/posts")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(postRequest)))
                .andDo(print())
                .andExpect(status().is(ErrorCode.INVALID_TOKEN.getHttpStatus().value()));
    }



    @Test
    @DisplayName("포스트 수정")
    @WithMockUser
    void post_edit() throws Exception {

        // given
        String editTitle = "수정 제목";
        String editBody = "수정 내용";
        PostRequest postEdit = new PostRequest(editTitle, editBody);

        // when
        when(postService.edit(any(), any(), any()))
                .thenReturn(postResponse);

        // then
        mockMvc.perform(put("/api/v1/posts/1")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(postEdit)))
                .andDo(print())
                .andExpect(jsonPath("$.result.message").exists())
                .andExpect(jsonPath("$.result.postId").exists())
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("포스트 삭제")
    @WithMockUser
    void post_delete() throws Exception {
        // when
        when(postService.delete(any(), any()))
                .thenReturn(postResponse);

        // then
        mockMvc.perform(delete("/api/v1/posts/1")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(jsonPath("$.result.message").exists())
                .andExpect(jsonPath("$.result.postId").exists())
                .andExpect(status().isOk());
    }
}